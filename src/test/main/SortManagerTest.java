package main;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

class SortManagerTest {

    private SortManager sortManager = new SortManager();

    //Testing if an array can be created given an array list and asserting an incomplete list or lists with wrong elements returns false.
    @Test
    void initializeArray() {
        ArrayList<String> testList = new ArrayList<>();
        testList.add("monkey");
        testList.add("rabbit");
        testList.add("hej");
        testList.add("horse");

        String[] expectedStringArray = {"hej", "horse", "monkey", "rabbit"};
        String[] incompleteArray = {"hej", "horse"};
        String[] unexpectedArray = {"hallå", "monkey", "rabbit"};

        Assert.assertArrayEquals("Could not create array", expectedStringArray, sortManager.initializeArray(testList));
        Assert.assertFalse("These should not be same", Arrays.equals(incompleteArray, sortManager.initializeArray(testList)));
        Assert.assertFalse("These should not be the same", Arrays.equals(unexpectedArray, sortManager.initializeArray(testList)));
    }

    //Testing if timSort can sort an array (using both insertionSort and mergeSort). Also asserting that the sorted one is not the same as the unsorted one.
    @Test
    void timSort() {
        String[] testArray = {"tjena", "hejsan", "tjo", "hejhej", "god dag", "hej", "morsning korsning", "Nagelfar", "Tribulation", "Izegrim", "Abbath",
                "Sportlov", "Satyricon", "Megadeth", "Once Human", "Slayer", "Meshuggah", "Mastodon", "Insomnium", "Arch Enemy", "Ufomammut", "Gojira", "Testament", "Myrkur", "Enslaved", "Obituary",
                "Helheim", "The Agonist", "Batushka", "horse", "pig", "dog", "cat", "monkey", "dove", "ant", "rabbit", "Sweden", "France", "Great Britain",
                "China", "India", "USA", "Poland", "Hungary", "Bangladesh", "Bolivia", "Japan", "Indonesia", "Australia", "Germany", "Ghana", "Canada", "Russia", "Ecuador",
                "South Africa", "Egypt", "Italy", "Spain", "Norway", "Mongolia", "Ireland", "Saudi Arabia", "Denmark", "Tchad", "Ethiopia", "Pelle", "Kalle", "Anders", "Lukas", "Niklas", "Albin"};
        String[] expectedArray = {"Abbath", "Albin", "Anders", "ant", "Arch Enemy", "Australia", "Bangladesh", "Batushka", "Bolivia", "Canada", "cat", "China",
                "Denmark", "dog", "dove", "Ecuador", "Egypt", "Enslaved", "Ethiopia", "France", "Germany", "Ghana", "god dag", "Gojira", "Great Britain", "hej", "hejhej", "hejsan",
                "Helheim", "horse", "Hungary", "India", "Indonesia", "Insomnium", "Ireland", "Italy", "Izegrim", "Japan", "Kalle", "Lukas", "Mastodon", "Megadeth", "Meshuggah",
                "Mongolia", "monkey", "morsning korsning", "Myrkur", "Nagelfar", "Niklas", "Norway", "Obituary", "Once Human", "Pelle", "pig", "Poland", "rabbit", "Russia",
                "Satyricon", "Saudi Arabia", "Slayer", "South Africa", "Spain", "Sportlov", "Sweden", "Tchad", "Testament", "The Agonist", "tjena", "tjo", "Tribulation", "Ufomammut", "USA"};
        String[] unexpectedArray = {"tjena", "hejsan", "tjo", "hejhej", "god dag", "hej", "morsning korsning", "Nagelfar", "Tribulation", "Izegrim", "Abbath",
                "Sportlov", "Satyricon", "Megadeth", "Once Human", "Slayer", "Meshuggah", "Mastodon", "Insomnium", "Arch Enemy", "Ufomammut", "Gojira", "Testament", "Myrkur", "Enslaved", "Obituary",
                "Helheim", "The Agonist", "Batushka", "horse", "pig", "dog", "cat", "monkey", "dove", "ant", "rabbit", "Sweden", "France", "Great Britain",
                "China", "India", "USA", "Poland", "Hungary", "Bangladesh", "Bolivia", "Japan", "Indonesia", "Australia", "Germany", "Ghana", "Canada", "Russia", "Ecuador",
                "South Africa", "Egypt", "Italy", "Spain", "Norway", "Mongolia", "Ireland", "Saudi Arabia", "Denmark", "Tchad", "Ethiopia", "Pelle", "Kalle", "Anders", "Lukas", "Niklas", "Albin"};
        Assert.assertArrayEquals("Could not sort.", expectedArray, sortManager.timSort(testArray));
        Assert.assertFalse("These should not be same.", Arrays.equals(unexpectedArray, sortManager.timSort(testArray)));

    }

    //Testing if insertionSort can sort given an unsorted array and that the result is not same as the unsorted array.
    @Test
    void insertionSort() {
        String[] testArray = {"horse", "pig", "dog", "cat", "monkey", "dove", "ant", "rabbit"};
        String[] expectedArray = {"ant","cat", "dog", "dove", "horse", "monkey", "pig", "rabbit"};
        String[] unexpectedArray = {"horse", "pig", "dog", "cat", "monkey", "dove", "ant", "rabbit"};
        Assert.assertArrayEquals("Could not sort correctly", expectedArray, sortManager.insertionSort(testArray, 0, testArray.length-1));
        Assert.assertFalse("These should not be same.", Arrays.equals(unexpectedArray, sortManager.insertionSort(testArray, 0, testArray.length-1)));
    }

    //Testing if merge sort can merge two arrays (a,c,e(indexes 0-2) and b,d,f(indexes 3-5)) made from one given their indexes and return the array sorted correctly.
    // Asserting that unmerged arrays are not returned as true.
    @Test
    void mergeSort() {
        String[] testArray = {"a","c", "e", "b", "d", "f"};
        String[] expectedArray = {"a", "b", "c", "d", "e", "f"};
        String[] unexpectedArray = {"a","c", "e", "b", "d", "f"};
        Assert.assertArrayEquals("Could not merge correctly", expectedArray, sortManager.mergeSort(testArray,0,5,2));
        Assert.assertFalse("These should not be the same.", Arrays.equals(unexpectedArray, sortManager.mergeSort(testArray, 0, 5, 2)));
    }


}