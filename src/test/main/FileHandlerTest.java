package main;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FileHandlerTest {

    // this test will check if the object files set the name and file path of text file
    // To run this test you need to give a path to your text file
    @Test
    void listAllFiles() throws FileNotFoundException {
        FileHandler fileHandler = new FileHandler();
        String expectedPath = "C:\\sortering\\src\\test\\testFiles\\testFiles.txt";
        fileHandler.listAllFiles(new File(expectedPath));
        String actualName = "testFiles.txt";
        String expectedName = fileHandler.getListOfFiles().get(0).getFileName();
        String actualPathPath = fileHandler.getListOfFiles().get(0).getFilePath();
        assertEquals(expectedName,actualName, "Expected to set name to object files");
        assertEquals(expectedPath,actualPathPath, "Expected to set filepath to object files");

    }

    // Test if the method read text file.
    // Consider that all words from text will be automatically sorted you therefore would a sorted list of word after reading your text file.
    @Test
    void readFile() throws FileNotFoundException {
        FileHandler fileHandler = new FileHandler();
        String filePath = "C:\\sortering\\src\\test\\testFiles\\testFiles.txt";
        File file = new File(filePath);
        fileHandler.listAllFiles(file);
        List<String> actual = new ArrayList<>();
        actual.add("i");
        actual.add("ignore");
        actual.add("more");
        actual.add("one");
        actual.add("time");
        assertEquals(fileHandler.getListOfFiles().get(0).getListOfWords(),actual, "Expect the to read word from text file");
    }
}