package main;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class SearchManagerTest {
    // test if the method would return the right index.
    @Test
    void binarySearchForFirstIndex() {
        SearchManager searchManager = new SearchManager();
        ArrayList<String> list = new ArrayList<>();
        list.add("Jesse".toLowerCase());
        list.add("Apple".toLowerCase());
        list.add("Hi".toLowerCase());
        list.add("To".toLowerCase());
        list.add("You".toLowerCase());
        list.add("zebra".toLowerCase());
        list.add("To".toLowerCase());
        list.add("When".toLowerCase());
        list.add("when".toLowerCase());
        list.add("Are".toLowerCase());
        list.add("here".toLowerCase());
        list.add("hello".toLowerCase());
        list.add("try".toLowerCase());
        list.add("to".toLowerCase());

        String[] list1 = searchManager.getSortManager().initializeArray(list);
        assertEquals(6,searchManager.binarySearchForFirstIndex(Arrays.asList(list1),"to"), "Expect to find the first occurrence of the index");
        assertEquals(-1,searchManager.binarySearchForFirstIndex(Arrays.asList(list1),"team1"), "Expect -1 when one word found in list");
    }

    @Test
    void binarySearchForLastIndex() {
        SearchManager searchManager = new SearchManager();
        ArrayList<String> list = new ArrayList<>();
        list.add("Jesse".toLowerCase());
        list.add("Apple".toLowerCase());
        list.add("Hi".toLowerCase());
        list.add("To".toLowerCase());
        list.add("You".toLowerCase());
        list.add("zebra".toLowerCase());
        list.add("To".toLowerCase());
        list.add("When".toLowerCase());
        list.add("when".toLowerCase());
        list.add("Are".toLowerCase());
        list.add("here".toLowerCase());
        list.add("hello".toLowerCase());
        list.add("try".toLowerCase());
        list.add("to".toLowerCase());

        String[] list1 = searchManager.getSortManager().initializeArray(list);
        assertEquals(8,searchManager.binarySearchForFirstIndex(Arrays.asList(list1),"to"), "Expect to find the last occurrence of index");
        assertEquals(-1,searchManager.binarySearchForFirstIndex(Arrays.asList(list1),"team1"), "Expect -1 when one word found in list");
    }

    // test if the result would return false if the word does not exit and if the result result return true
    // when the word does exist.
    @Test
    void checkResult() {
        SearchManager searchManager = new SearchManager();
        assertTrue(searchManager.checkResult(0), "Expect true when the result exist on the list where result => 0");
        assertFalse(searchManager.checkResult(-1), "Expect false when the result does not exist on the list where result=-1");
    }

    @Test
    void countWord() {
        SearchManager searchManager = new SearchManager();
        assertEquals(2,searchManager.countWord(1,2), "Expect 2");
        assertEquals(1,searchManager.countWord(1,1), "Expect 1");
        assertEquals(1,searchManager.countWord(0,0), "Expect 1");
    }

    @Test
    void mostFoundWord() {
        SearchManager searchManager = new SearchManager();

        List<Integer> integers = new ArrayList<>();
        integers.add(10);
        integers.add(11);
        integers.add(5);
        integers.add(7);
        integers.add(11);
        integers.add(12);

        List<Integer> integers1 = new ArrayList<>();
        integers1.add(10);
        integers1.add(11);
        integers1.add(5);
        integers1.add(7);
        integers1.add(11);
        integers1.add(11);

        List<Integer> integers2 = new ArrayList<>();
        integers2.add(2);
        integers2.add(2);
        integers2.add(2);
        integers2.add(2);
        integers2.add(2);
        integers2.add(2);

        List<Integer> integers3 = new ArrayList<>();
        integers3.add(20);
        integers3.add(11);
        integers3.add(5);
        integers3.add(7);
        integers3.add(11);
        integers3.add(12);

        assertEquals(5,searchManager.mostFoundWord(integers), "Expect the index that contain the largest value in the list");
        assertEquals(-1,searchManager.mostFoundWord(integers1), "Expect -1 when there are duplicate in the lis number in the list");
        assertEquals(-1,searchManager.mostFoundWord(integers2), "Expect -1 when no largest number in the list");
        assertEquals(0,searchManager.mostFoundWord(integers3), "Expect 0 when largest number is in first position of an the list");
    }

}