package main;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class WriteToFileManagerTest {

    // this test check if the is being create with the given path.
    // To test this method you need to give the expectedfilePath and the actualfilePath where the actual text file will be created
    @Test
    void createTextFile() throws IOException {
        WriteToFileManager writeToFileManager = new WriteToFileManager(new FileHandler());
        String actualfilePath = "C:\\sortering\\src\\test\\testFiles\\testFiles1.txt";
        String expectedfilePath = "C:\\sortering\\src\\test\\testFiles\\testFiles.txt";
        File file = new File(actualfilePath);
        FileWriter fileWriter = new FileWriter(file);
        writeToFileManager.getFileHandler().listAllFiles(file);
        writeToFileManager.createTextFile(expectedfilePath,0);
        writeToFileManager.writeToFile(fileWriter, writeToFileManager.getFileHandler().getListOfFiles().get(0).getListOfWords());
        assertTrue(new File(actualfilePath).exists());
    }
}