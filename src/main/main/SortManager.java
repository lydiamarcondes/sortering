package main;

import java.util.List;

public class SortManager {

    //Size for sub arrays.
    int run = 32;

    //Converting given array list into array.
    String[] initializeArray(List toSort) {

        String[] originalArray = new String[toSort.size()];
        toSort.toArray(originalArray);
        timSort(originalArray);
        return originalArray;
    }

    //Implementing TimSort.
    String[] timSort(String[] arrayToSort) {

        //Dividing array into runs, sorting them using insertion sort implementation.
        for (int i = 0; i < arrayToSort.length; i += run) {
            insertionSort(arrayToSort, i, Math.min((i + run-1), (arrayToSort.length - 1)));
        }

        //Defining next sub array size, doubling it until it matches the size of the original array.
        for (int subArraySize = run; subArraySize < arrayToSort.length; subArraySize = 2 * subArraySize) {

            //Defining where left run starts for each run.
            for (int leftStarting = 0; leftStarting < arrayToSort.length; leftStarting += 2 * subArraySize) {

                //Defining where the left run ends.
                int leftEnding = leftStarting + subArraySize - 1;

                //Making sure middle is not greater than size of original array.
                if (leftEnding > arrayToSort.length){
                    leftEnding = arrayToSort.length;
                }
                //Defining where the right run ends by picking the smallest of (starting point for left run + 2* length of sub array) and (length of entire array -1).
                int rightEnding = Math.min((leftStarting + (2 * subArraySize - 1)), (arrayToSort.length - 1));

                //Calling merge sort implementation to merge runs.
                try {
                    mergeSort(arrayToSort, leftStarting, rightEnding, leftEnding);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        return arrayToSort;
    }

    //Sorting run using insertion sort algorithm.
    String[] insertionSort(String[] arrayToSort, int left, int right) {

        // Going through all elements in given run. Each turn assigns current index to a temp variable and index-1 to j.
        for (int i = left + 1; i <= right; i++) {
            String temp = arrayToSort[i];
            int j = i - 1;

            //Putting the string at index j to index j+1 and decreasing j with 1 as long as j is greater than or equal to left index and string at j is greater than the temp string.
            while (j >= left && arrayToSort[j].compareToIgnoreCase(temp) > 0) {
                arrayToSort[j + 1] = arrayToSort[j];
                j--;
            }

            //Setting the string at index j+1 to whatever temp is currently.
            arrayToSort[j + 1] = temp;

        }
        return arrayToSort;
    }



    //Merging the sorted runs into one array two at a time (left and right index).
    String[] mergeSort(String[] entireArray, int leftIndex, int rightIndex, int middleIndex) {

        //Defining lengths for both arrays based on values in parameters.
        int leftLength = middleIndex - leftIndex +1;
        int rightLength = rightIndex - middleIndex;

        //Preventing negative array length.
        if(middleIndex > rightIndex){
            rightLength = 0;
        }

        //Creating arrays to merge, with lengths defined above.
        String[] leftRun = new String[leftLength];
        String[] rightRun = new String[rightLength];

        //Copying values to left array.
        for (int i = 0; i < leftLength && leftIndex + i < entireArray.length; i++) {
            leftRun[i] = entireArray[leftIndex + i];
        }
        //Copying values to right array.
        for (int i = 0; i < rightLength; i++) {
            rightRun[i] = entireArray[middleIndex + 1 + i];
        }

        //Initializing index variables.
        int a = 0;
        int b = 0;
        int c = leftIndex;

        //Merging into bigger array.
        while (a < leftLength && b < rightLength) {

            //Comparing element in leftRun[a] with rightRun[b] to see who comes first and put in index c of entireArray.
            if (leftRun[a].compareToIgnoreCase(rightRun[b]) <= 0) {
                entireArray[c] = leftRun[a];
                a++;
            } else {
                entireArray[c] = rightRun[b];
                b++;
            }
            c++;
        }

        //Pass on already sorted elements from left run.
        while (a < leftLength && c < entireArray.length) {
            entireArray[c] = leftRun[a];
            c++;
            a++;
        }

        //Pass on already sorted elements from right run.
        while (b < rightLength) {
            entireArray[c] = rightRun[b];
            c++;
            b++;
        }
        return entireArray;
    }
}
