package main;

import java.util.*;

public class Files {
    // File information
    private String fileName;
    private String filePath;
    private List<String> listOfWords = new ArrayList<>();
    public Files() {
    }

    List<String> getListOfWords() {
        return listOfWords;
    }

    public void setListOfWords(List<String> listOfWords) {
        this.listOfWords = listOfWords;
    }

    void setFileName(String fileName) {
        this.fileName = fileName;
    }

    String getFileName() {
        return fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
