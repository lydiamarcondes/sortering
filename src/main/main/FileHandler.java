package main;

import java.io.*;
import java.util.*;

public class FileHandler {

    private List<Files> listOfFiles = new ArrayList<>();
    BufferedReader bufferedReader;
    private SortManager sortManager = new SortManager();


    // list all the existing files from folder
    // recursively find all text file on given directory, File root
    void listAllFiles(File root) throws FileNotFoundException {
        File[] listOfFiles = root.listFiles();
        int fileIndex = this.listOfFiles.size();
        // if there are more than one files in the given directory. Loop through all the files and look for text file
        // if there is text file, then create an object of Files where the object get its name and path
        if (listOfFiles != null && listOfFiles.length > 0) {
            for (int i = 0; i < Objects.requireNonNull(root.listFiles()).length; i++) {
                String iName = listOfFiles[i].getName();
                if (listOfFiles[i].isFile()) {
                    if (iName.endsWith(".txt") || iName.endsWith(".TXT")) {
                        this.listOfFiles.add(new Files());
                        this.listOfFiles.get(fileIndex).setFileName(iName);
                        this.listOfFiles.get(fileIndex).setFilePath(listOfFiles[i].getAbsolutePath());
                        readFile(listOfFiles[i], fileIndex);
                        fileIndex++;
                    }
                } else if (listOfFiles[i].isDirectory()) {
                    listAllFiles(listOfFiles[i]);
                }
            }
        } else if (root.exists()) {
            this.listOfFiles.add(new Files());
            this.listOfFiles.get(fileIndex).setFileName(root.getName());
            this.listOfFiles.get(fileIndex).setFilePath(root.getAbsolutePath());
            readFile(root, fileIndex);
        } else
            throw new FileNotFoundException(" File does not exist");
    }

    // this method will read all the text files and enter each word into an array list and sort it.
    void readFile(File file, int filePosition) {
        try {
            System.out.println("Read file " + file.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            String defaultEncoding = inputStreamReader.getEncoding();
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream (file), defaultEncoding));
            String line;

            while ((line = bufferedReader.readLine()) != null){
                String[] words = line.split(" ");
                for(String stringInArray : words){
                    String stringToAdd = stringInArray.replaceAll("[-+.^:,!�`';|<>��()��?_�]*","");
                    listOfFiles.get(filePosition).getListOfWords().add(stringToAdd.toLowerCase());
                }
            }
            System.out.println("Array list created: " + listOfFiles.get(filePosition).getListOfWords());
            listOfFiles.get(filePosition).setListOfWords(Arrays.asList(sortManager.initializeArray(listOfFiles.get(filePosition).getListOfWords())));
            System.out.println("Sorted: " + listOfFiles.get(filePosition).getListOfWords());

        } catch (FileNotFoundException ex) {
            System.out.println("Not Found! Please check your file");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            closeReader();
        }
    }
    // Close bufferedReader to avoid error
    void closeReader(){
        try {
            bufferedReader.close();
        }catch (IOException io){
            System.out.println(io + " ");
        }
    }

    public List<Files> getListOfFiles() {
        return listOfFiles;
    }
}
