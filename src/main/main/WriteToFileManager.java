package main;

import java.io.*;
import java.util.*;

public class WriteToFileManager {
    private BufferedWriter bufferedWriter;

    private FileHandler fileHandler;

    WriteToFileManager(FileHandler fileHandler) {
        this.fileHandler = fileHandler;
    }

    //Show alternatives
    void start() {
        System.out.println("Do you want to save sorted data in the existing text file or create a new one?\n" +
                "Press 1: To save in an existing file\n" +
                "Press 2: To save in a new text File");
        Scanner sc = new Scanner(System.in);
        while (true) {
            try {
                byte userInput = sc.nextByte();
                if (userInput == 1) {
                    print();
                    saveToExistingTextFile();
                    break;
                } else if (userInput == 2) {
                    System.out.println("Enter your file path to create a new text file");
                    String fileLocation = sc.next();
                    while(!fileLocation.endsWith(".txt")){
                        System.out.println("Please give your text file a name and end with .txt. Example Yourpath\\textfilename.txt");
                        fileLocation = sc.next();
                    }
                    System.out.println("Enter file position you want to save");
                    int filePosition = sc.nextInt();
                    createTextFile(fileLocation, filePosition);
                    break;
                } else {
                    System.out.println("Invalid alternative");

                }
            } catch (InputMismatchException im) {
                System.out.println("Invalid input");
                break;
            }
        }

    }
    // create file and write to the given file path. Check if the file wanted to save exist or not before do the operation
    void createTextFile(String fileLocation, int filePosition) {

        try {
            File file = new File(fileLocation);
            // check if file exist or not before create new file to save
            if(filePosition >= fileHandler.getListOfFiles().size() || filePosition < 0) {
                System.out.println("Could not find file you want to save! Check if file exist");
            } else{
                if (file.createNewFile()) {
                    System.out.println("File is created!");
                }
                FileWriter fileWriter = new FileWriter(file);
                writeToFile(fileWriter, fileHandler.getListOfFiles().get(filePosition).getListOfWords());
            }
        } catch (IOException io) {
            System.out.println(io + " ");
        } catch (IndexOutOfBoundsException id){
            System.out.println(id + " Can not find file, Check if file file exist");
        }
    }
    //
    void saveToExistingTextFile(){
        try {
            for (int i = 0; i < fileHandler.getListOfFiles().size(); i++) {
                File file = new File(fileHandler.getListOfFiles().get(i).getFilePath());
                FileWriter fileWriter = new FileWriter(file);
                writeToFile(fileWriter, fileHandler.getListOfFiles().get(i).getListOfWords());
            }
        }catch (IOException io){
            System.out.println(io + ": ");
        }
    }
    // Write to a file
    void writeToFile(FileWriter file, List<String> listOfStrings) {
        bufferedWriter = new BufferedWriter(file);

        try {
            for (String a : listOfStrings) {
                bufferedWriter.write(a);
                bufferedWriter.newLine();
            }
        } catch (IOException io) {
            System.out.println(io + " Could not find location");
        }
        closeFile();

    }
    // close bufferedWriter to not occur error
    void closeFile() {
        try {
            bufferedWriter.close();
        } catch (IOException io) {
            System.out.println("Not Found");
        }
    }
    // print information of Object Files which represent a text file
    void print() {
        if (fileHandler.getListOfFiles().size() > 0) {
            System.out.println("Existing File \n");
            for (int i = 0; i < fileHandler.getListOfFiles().size(); i++) {
                System.out.println("file position " + i + ": File Name: " + fileHandler.getListOfFiles().get(i).getFileName());
            }
        } else {
            System.out.println("Empty list File: No file found");
        }
    }
    public FileHandler getFileHandler() {
        return fileHandler;
    }
}
