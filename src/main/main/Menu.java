package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

class Menu {
    private static Menu menu = null;
    private FileHandler fileHandler = new FileHandler();
    private WriteToFileManager writeToFileManager = new WriteToFileManager(fileHandler);

    private Menu() {
        menu();
    }

    void menu() {
        Scanner sc = new Scanner(System.in);
        boolean continueLoop = true;
        while (continueLoop) {
            while (continueLoop) {
                System.out.println("\nSearch\n" +
                        "Press 1: Search words\n" +
                        "Press 2: Insert File\n" +
                        "Press 3: Save file\n" +
                        "Press 4: See file information\n" +
                        "Press 5: Exit");
                try {
                    byte userInput = sc.nextByte();
                    sc.nextLine();
                    if (userInput == 1) {
                        SearchManager searchManager = new SearchManager();
                        searchManager.setFileHandler(fileHandler);
                        System.out.println("Search words");
                        String input = sc.next();
                        searchManager.search(input);
                    } else if (userInput == 2) {
                        System.out.println("Choose your file location");
                        String stringInput = sc.nextLine();
                        fileHandler.listAllFiles(new File(stringInput));
                    } else if (userInput == 3) {
                        writeToFileManager.start();
                    }else if(userInput == 4){
                        writeToFileManager.print();
                    }
                    else if (userInput == 5)
                        continueLoop = false;
                    else
                        System.out.println("Invalid alternative");
                } catch (InputMismatchException im) {
                    System.out.println(im + ": Invalid input");
                    sc.nextLine();
                } catch (FileNotFoundException fn) {
                    System.out.println(fn + " ");
                }
            }
        }
    }

    static void getMenu() {
        if (menu == null)
            menu = new Menu();
    }
}
