package main;

import java.io.FileNotFoundException;
import java.util.*;

public class SearchManager {
    private FileHandler fileHandler;
    private SortManager sortManager = new SortManager();

    // this method will check the existing of the searching word in all text file.
    void search(String userInput) throws FileNotFoundException {
        // check if there are any existing file before starting search operation
        if (fileHandler.getListOfFiles() != null && fileHandler.getListOfFiles().size() > 0) {
            List<Integer> listOfAmountWord = new ArrayList<>();
            int totalAmount;
            int hasWord = 0;
            // go through all files object and look after a searching word
            for (int i = 0; i < fileHandler.getListOfFiles().size(); i++) {
                int firstOccurrence = binarySearchForFirstIndex(fileHandler.getListOfFiles().get(i).getListOfWords(), userInput);
                int lastOccurrence = binarySearchForLastIndex(fileHandler.getListOfFiles().get(i).getListOfWords(), userInput);
                // if the word exist then call method countWord() to check how many found word in the ListWord. Add to listOfAmountWord will save all found word
                // to later use it for checking which file has the most found word.
                // if no word exist in ListWord then add 0 to listOfAmountWord
                if (checkResult(firstOccurrence)) {
                    hasWord = firstOccurrence;
                    totalAmount = countWord(firstOccurrence, lastOccurrence);
                    listOfAmountWord.add(totalAmount);
                    System.out.println("found " + fileHandler.getListOfFiles().get(i).getListOfWords().get(firstOccurrence) + " in " + fileHandler.getListOfFiles().get(i).getFileName() + "\n"
                            + fileHandler.getListOfFiles().get(i).getFileName() + " has " + totalAmount + "\n");
                } else {
                    System.out.println("Not found in " + fileHandler.getListOfFiles().get(i).getFileName() + "\n");
                    listOfAmountWord.add(0);
                }
            }
            // the if condition here to make sure only print out most found word when the result exist in the Files Object.
            if(checkResult(hasWord)) {
                int index = mostFoundWord(listOfAmountWord); // return an index of a file that has the most found word.
                if (index != -1 && fileHandler.getListOfFiles().size() > 1) {
                    System.out.println(fileHandler.getListOfFiles().get(index).getFileName() + " has the most found word " + "\n");
                }
            }
        } else
            throw new FileNotFoundException("Not found! Check if file has been inserted");
    }

    // Binary search for occurrence of first index
    int binarySearchForFirstIndex(List<String> list, String userInput) {
        int min = 0;
        int max = list.size() - 1;
        int ans = -1;
        while (min <= max) {
            int mid = min + (max - min + 1) / 2;
            if (userInput.compareTo(list.get(mid)) > 0)
                min = mid + 1;
            else if (userInput.compareTo(list.get(mid)) < 0)
                max = mid - 1;

            else {
                ans = mid;
                max = mid - 1;
            }
        }
        return ans;
    }
// Binary search for occurrence of last index
    int binarySearchForLastIndex(List<String> list, String userInput) {
        int min = 0;
        int max = list.size() - 1;
        int ans = -1;
        while (min <= max) {
            int mid = min + (max - min + 1) / 2;
            if (userInput.compareTo(list.get(mid)) > 0)
                min = mid + 1;
            else if (userInput.compareTo(list.get(mid)) < 0)
                max = mid - 1;

            else {
                ans = mid;
                min = mid + 1;
            }
        }
        return ans;
    }

    // Check if the result exist
    boolean checkResult(int result) {
        return result != -1;
    }
    // count method
    int countWord(int pOne, int pTwo) {
        int totalAmount = 0;
        for (int i = pOne; i <= pTwo; i++) {
            totalAmount++;
        }
        return totalAmount;
    }

    // return an index of highest number in indexOfFoundWord
    int mostFoundWord(List<Integer> indexOfFoundWord) {
        int index = -1;
        if (!indexOfFoundWord.isEmpty()) {
            int max = indexOfFoundWord.get(0);
            index = 0;
            for (int i = 1; i < indexOfFoundWord.size(); i++) {
                if (indexOfFoundWord.get(i) > max) {
                    max = indexOfFoundWord.get(i);
                    index = i;
                } else if(indexOfFoundWord.get(i).equals(max))
                    index = -1;
            }
        }
        return index;
    }

    public void setFileHandler(FileHandler fileHandler) {
        this.fileHandler = fileHandler;
    }

    public SortManager getSortManager() {
        return sortManager;
    }
}
